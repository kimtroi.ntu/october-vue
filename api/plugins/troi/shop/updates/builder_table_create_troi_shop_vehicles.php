<?php namespace Troi\Shop\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTroiShopVehicles extends Migration
{
    public function up()
    {
        Schema::create('troi_shop_vehicles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 191)->nullable();
            $table->string('slug', 191)->nullable();
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('troi_shop_vehicles');
    }
}
