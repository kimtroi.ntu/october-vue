<?php namespace Troi\Shop\Models;

use Model;

/**
 * Model
 */
class Vehicle extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    // Relations

    public $belongsToMany = [
        'locations' => [
            'Troi\Shop\Models\Location',
            'table' => 'troi_shop_vehicles_locations',
            'order' => 'title'
        ]
    ];

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'troi_shop_vehicles';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
