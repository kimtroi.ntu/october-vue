<?php

use Troi\Shop\Models\Vehicle;
use Troi\Shop\Models\Location;

Route::get('/vehicles', function() {
    return Vehicle::with(['image', 'locations'])->get();
});

Route::get('/vehicles/filter/{id}', function($id) {
    $vehicles = Vehicle::whereHas('locations', function($query) use ($id){
        $query->where('id', '=', $id);
    })->get();

    return $vehicles;
});

Route::get('/locations', function() {
    return Location::all();
});
